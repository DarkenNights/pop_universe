const nextTranslate = require('next-translate')

module.exports = {
  ...nextTranslate(),
  async headers() {
    return [
      {
        source: '/(.*?)',
        headers: [
          {
            key: 'Authorization',
            value: 'Bearer',
          },
        ],
      },
    ]
  },
  images: {
    domains: ['pop-collection.s3.eu-west-3.amazonaws.com'],
  },
}
