import axios from 'axios'

class AuthService {
  constructor() {
    this.BASE_URL = process.env.NEXT_PUBLIC_BASE_URL + '/api/auth/'
  }

  async register(name, email, password) {
    return axios({
      method: 'POST',
      url: this.BASE_URL + 'register',
      headers: {
        'Content-Type': 'application/json',
      },
      data: {
        name,
        email,
        password,
      },
    })
  }
}

export default new AuthService()
