import Image from 'next/image'
import { useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import windowSize from '../utils/windowSize'
import Link from 'next/link'
import Heart from './icons/Heart'
import Notification from './icons/Notification'
import Cart from './icons/Cart'
import User from './icons/User'

export default function Header() {
  const router = useRouter()
  let navTitle = ''
  const size = windowSize()

  if (router.pathname === '/') {
    if (size.width < 992) {
      navTitle = (
        <div className="nav-title">Pop Universe, la plateforme dédiée aux Pop Cultures</div>
      )
    } else {
      navTitle = <div className="nav-title">Choisissez votre Pop Culture</div>
    }
  }

  return (
    <header>
      <nav className="nav-primary">
        <div className="menu-burger">
          <Image src="/images/icons/burger.svg" alt="Icône burger" width={35} height={35} />
        </div>
        <div className="logo">
          {size.width >= 375 ? (
            <Image src="/logo.svg" alt="Logo de Pop Universe" width={168} height={70} />
          ) : (
            <Image src="/logo-mobile.png" alt="Logo de Pop Universe" width={50} height={50} />
          )}
        </div>
        {size.width >= 576 ? (
          <div className="header-search-container">
            <input type="text" />
            <button>
              <Image
                src="/images/icons/search-white.svg"
                width={25}
                height={25}
                alt="Icone de recherche blanc"
              />
            </button>
          </div>
        ) : (
          <></>
        )}
        <ul>
          {size.width < 576 ? (
            <li>
              <Image
                src="/images/icons/search.svg"
                width={25}
                height={25}
                alt="Icone de recherche"
              />
            </li>
          ) : (
            <li>
              <Heart width={25} height={25} />
              <div>Mes listes</div>
            </li>
          )}
          <li>
            <Notification width={25} height={25} />
            <div>Notifications</div>
          </li>
          <li>
            <Cart width={25} height={25} />
            <div>Mon panier</div>
          </li>
          <li>
            <User width={25} height={25} />
            <div>Mon compte</div>
          </li>
        </ul>
      </nav>
      <nav className="nav-secondary">
        {navTitle}
        <div className="nav-pop-culture-container">
          <Link href="/">
            <a>
              <div className="nav-pop-culture">
                <Image
                  src="/images/pop_culture/fantasy/fantasy_icon.svg"
                  width={34}
                  height={40}
                  alt="Icône de la pop culture Fantasy"
                />
                <div className="nav-pop-culture-title">
                  Pop Culture
                  <br />
                  Fantasy
                </div>
              </div>
            </a>
          </Link>
          <hr />
          <Link href="/">
            <a>
              <div className="nav-pop-culture">
                <Image
                  src="/images/pop_culture/retro/retro_icon.svg"
                  width={50}
                  height={40}
                  alt="Icône de la pop culture Retro"
                />
                <div className="nav-pop-culture-title">
                  Pop Culture
                  <br />
                  Retro
                </div>
              </div>
            </a>
          </Link>
          <hr />
          <Link href="/">
            <a>
              <div className="nav-pop-culture">
                <Image
                  src="/images/pop_culture/gaming/gaming_icon.svg"
                  width={50}
                  height={40}
                  alt="Icône de la pop culture Gaming"
                />
                <div className="nav-pop-culture-title">
                  Pop Culture
                  <br />
                  Gaming
                </div>
              </div>
            </a>
          </Link>
          <hr />
          <Link href="/">
            <a>
              <div className="nav-pop-culture">
                <Image
                  src="/images/pop_culture/japan/japan_icon.svg"
                  width={50}
                  height={40}
                  alt="Icône de la pop culture Japonaise"
                />
                <div className="nav-pop-culture-title">
                  Pop Culture
                  <br />
                  Japonaise
                </div>
              </div>
            </a>
          </Link>
          <hr />
          <Link href="/">
            <a>
              <div className="nav-pop-culture">
                <Image
                  src="/images/pop_culture/comics/comics_icon.svg"
                  width={50}
                  height={40}
                  alt="Icône de la pop culture Comics"
                />
                <div className="nav-pop-culture-title">
                  Pop Culture
                  <br />
                  Comics
                </div>
              </div>
            </a>
          </Link>
          <hr />
          <Link href="/">
            <a>
              <div className="nav-pop-culture">
                <Image
                  src="/images/pop_culture/sci-fi/sci-fi_icon.svg"
                  width={50}
                  height={40}
                  alt="Icône de la pop culture Science-Fiction"
                />
                <div className="nav-pop-culture-title">
                  Pop Culture
                  <br />
                  Science-Fiction
                </div>
              </div>
            </a>
          </Link>
        </div>
      </nav>
    </header>
  )
}
