import styles from '../styles/components/Pagination.module.scss'
import useTranslation from 'next-translate/useTranslation'
import windowSize from '../utils/windowSize'

export default function Pagination() {
    const { t, lang } = useTranslation('common')
    const size = windowSize()

    const previous = size.width > 576 ? t('previous') : '<'
    const next = size.width > 576 ? t('next') : '>'

    return (
        <div className={styles.container}>
            <div>{previous}</div>
            <div>1</div>
            <div>2</div>
            <div>3</div>
            <div className={styles.dots}>...</div>
            <div>10</div>
            <div>{next}</div>
        </div>
    )
}
