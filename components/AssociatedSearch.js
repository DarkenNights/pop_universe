import styles from '../styles/components/AssociatedSearch.module.scss'
import useTranslation from 'next-translate/useTranslation'
import Image from 'next/image'

export default function Pagination({ text }) {
    return (
        <div className={styles.container}>
            <Image src="/images/icons/search.svg" width={25} height={25} alt="Icone de recherche" />
            <div className={styles.text}>{text}</div>
        </div>
    )
}
