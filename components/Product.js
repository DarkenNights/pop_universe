import styles from '../styles/components/Product.module.scss'
import Image from 'next/image'
import ButtonAddList from './buttons/ButtonAddList'
import ButtonAddCart from './buttons/ButtonAddCart'
import ButtonAddWishlist from './buttons/ButtonAddWishlist'
import Link from 'next/link'

export default function Product() {
    return (
        <div className={styles.product}>
            <Link href="/catalogue" passHref>
                <div className={styles['image-container']}>
                    <Image src="/images/products/yoda.png" width={153} height={147} layout="responsive" alt="Pop Universe produit" />
                    <div className={styles['image-placeholder']}>
                        <div className={styles['placeholder-text']}>
                            <Image src="/images/pop_culture/sci-fi/sci-fi_icon.svg" width={30} height={30} alt="Pop culture science-fiction Pop Universe" />
                            <span>
                                Pop Culture <br /> Science-fiction
                            </span>
                        </div>
                        <button className={styles['placeholder-button']}>En savoir plus</button>
                    </div>
                    <div className={styles['popculture-container']}>
                        <Image src="/images/pop_culture/sci-fi/sci-fi_icon.svg" width={30} height={30} alt="Pop culture science-fiction Pop Universe" />
                    </div>
                </div>
            </Link>

            <div className={styles.name}>Poupée animée Bébé Yoda</div>
            <div className={styles.infos}>
                <div className={styles.price}>29,99 €</div>
                <ButtonAddList width={20} height={20} />
                <ButtonAddWishlist width={20} height={20} />
                <ButtonAddCart width={15} height={15} />
            </div>
        </div>
    )
}
