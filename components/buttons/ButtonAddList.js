import styles from '../../styles/components/buttons/ButtonAddList.module.scss'
import Image from 'next/image'

export default function ButtonAddList({ width, height }) {
    return (
        <div className={styles.container + ' AddList-container'}>
            <div className="icon">
                <Image src="/images/icons/add.svg" width={width} height={height} alt="Pop Universe bouton ajouter" />
            </div>

            <style jsx>
                {`
                    .AddList-container {
                        width: ${width}px;
                        height: ${height}px;
                        line-height: ${height / 2}px;
                    }
                `}
            </style>
        </div>
    )
}
