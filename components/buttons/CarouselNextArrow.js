export default function CarouselNextArrow(props) {
    const { className, style, onClick, background = 'transparent' } = props
    return <div className={className} style={{ display: 'block', background: background }} onClick={onClick} />
}
