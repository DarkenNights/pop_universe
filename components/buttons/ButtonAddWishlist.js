import styles from '../../styles/components/buttons/ButtonAddWishlist.module.scss'
import Image from 'next/image'

export default function AddList({ width, height }) {
    return (
        <div className={styles.container + ' AddWishlist-container'}>
            <div className="icon">
                <Image src="/images/icons/heart.svg" width={width} height={height} alt="Pop Universe bouton favoris" />
            </div>

            <style jsx>
                {`
                    .AddWishlist-container {
                        width: ${width}px;
                        height: ${height}px;
                        line-height: ${height / 2}px;
                    }
                `}
            </style>
        </div>
    )
}
