import styles from '../../styles/components/buttons/ButtonAddCart.module.scss'
import Cart from '../icons/Cart'

export default function ButtonAddCart({ width, height }) {
    return (
        <div className={styles.container + ' AddCart-container'}>
            <Cart className={'add-cart-icon'} width={width} height={height} />
            <div className={styles.text}>Ajouter</div>

            <style jsx global>
                {`
                    .add-cart-icon .icon-stroke {
                        fill: #ffffff;
                    }
                `}
            </style>
        </div>
    )
}
