export default function CarouselPrevArrow(props) {
    const { className, style, onClick, background = 'transparent' } = props
    return <div className={className} style={{ ...style, display: 'block', background: background }} onClick={onClick} />
}
