import styles from '../../../styles/components/sidebars/catalog/CatalogFilters.module.scss'
import 'rc-slider/assets/index.css'
import useTranslation from 'next-translate/useTranslation'
import FilterContainer from './CatalogFilterContainer'
import Checkbox from '../../forms/Checkbox'
import Cancel from '../../icons/Cancel'
import Slider, { SliderTooltip } from 'rc-slider'
const { createSliderWithTooltip } = Slider
const Range = createSliderWithTooltip(Slider.Range)
import { useState } from 'react'

export default function CatalogFilters({ toggleClass }) {
    const { t, lang } = useTranslation('catalog')
    const [minPrice, setMinPrice] = useState(0)
    const [maxPrice, setMaxPrice] = useState(1000)

    return (
        <>
            <div className={styles['container-title']}>
                <div className={styles['cancel-icon']} onClick={toggleClass}>
                    <Cancel width={20} height={20} />
                </div>
            </div>
            <FilterContainer title={t('filters.title.categories')}>
                <Checkbox text={'Vêtements'} />
                <Checkbox text={'Vêtements'} />
                <Checkbox text={'Vêtements'} />
                <Checkbox text={'Vêtements'} />
                <Checkbox text={'Vêtements'} />
                <Checkbox text={'Vêtements'} />
                <Checkbox text={'Vêtements'} />
                <Checkbox text={'Vêtements'} />
            </FilterContainer>
            <FilterContainer title={t('filters.title.price')}>
                <div className={styles['price-container']}>
                    <Range min={0} max={1000} defaultValue={[minPrice, maxPrice]} tooltip={true} tipFormatter={(value) => `${value} €`} marks={{ 0: 0, 1000: 1000 }} />
                </div>
            </FilterContainer>
            <FilterContainer title={t('filters.title.types')}>
                <Checkbox text={'Neufs'} />
                <Checkbox text={'Occasions'} />
                <Checkbox text={'Échanges'} />
            </FilterContainer>
            <FilterContainer title={t('filters.title.franchises')}>
                <Checkbox text={'Batman'} />
                <Checkbox text={'Batman'} />
                <Checkbox text={'Batman'} />
                <Checkbox text={'Batman'} />
                <Checkbox text={'Harry Potter'} />
                <Checkbox text={'Harry Potter'} />
                <Checkbox text={'Harry Potter'} />
                <Checkbox text={'World Of Warcraft'} />
            </FilterContainer>
        </>
    )
}
