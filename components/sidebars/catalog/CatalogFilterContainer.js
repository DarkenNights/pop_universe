import styles from '../../../styles/components/sidebars/catalog/CatalogFilterContainer.module.scss'
import { useState } from 'react'
import ArrowDown from '../../icons/ArrowDown'

export default function CatalogFilterContainer({ title, isCenter = false, isScrollable = false, children }) {
    const [isActive, setActive] = useState(true)

    const toggleClass = () => {
        setActive(!isActive)
    }

    return (
        <div className={styles.filter + ' ' + (isCenter ? ' ' + styles.center : '') + ' ' + (isActive ? ' ' + styles.active : '') + ' ' + (isScrollable ? ' ' + styles.scrollable : '')}>
            <div className={styles.header} onClick={toggleClass}>
                <div className={styles.title}>{title}</div>
                <div className={styles.arrow}>
                    <ArrowDown width={10} height={10} />
                </div>
            </div>
            <div className={styles.content}>{children}</div>
        </div>
    )
}
