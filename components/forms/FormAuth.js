import styles from '../../styles/components/forms/FormAuth.module.scss'

export default function FormAuth({ buttonText, children, action, method, submitFunction }) {
  return (
    <form className={styles['form-auth']} method={method} action={action}>
      {children}
      <button type="submit" className={styles['auth-button'] + ' btn'} onClick={() => submitFunction()}>
        {buttonText}
      </button>
    </form>
  )
}

FormAuth.defaultProps = {
  method: 'POST',
  action: '',
}
