import styles from '../../styles/components/forms/Input.module.scss'

export default function Input({ type, placeholder, value, handleChange }) {
  return (
    <input
      type={type}
      placeholder={placeholder}
      onChange={() => handleChange}
      className={styles['auth-input']}
    />
  )
}
