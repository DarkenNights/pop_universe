import Image from 'next/image'
import Facebook from "./icons/Facebook";
import Instagram from "./icons/Instagram";

export default function Footer() {
  return <footer>
    <div className="footer-texts">
      <div className='footer-logo'>
        <Image src='/logo-white.svg' width={140} height={45} alt="Pop Universe logo"/>
        <div className="footer-description">
          Achetez, vendez et échangez tous les produits <br /> issus de la Pop Culture.
        </div>
      </div>
      <div className="footer-shop">
        <h3>Boutique en ligne</h3>
        <ul>
          <li>Suivi de commande</li>
          <li>Retours</li>
          <li>Option de paiement</li>
          <li>Contactez-nous</li>
        </ul>
      </div>
      <div className="footer-infos">
        <h3>Informations</h3>
        <ul>
          <li>Carte cadeaux</li>
          <li>Newsletter</li>
          <li>Devenez membre</li>
          <li>Avis utilisateurs</li>
        </ul>
      </div>
      <div className="footer-contact">
        <h3>Aides et contact</h3>
        <ul>
          <li>Centre d&apos;aides</li>
          <li>Forum</li>
          <li>Nous contacter</li>
          <li>contact@popuniverse.fr</li>
        </ul>
      </div>
    </div>
    <div className="footer-social">
      <Facebook width={25} height={25} color={"#ffffff"} />
      <Instagram width={25} height={25} color={"#ffffff"} />
    </div>
    <div className="footer-copyright">
      © 2021. POP UNIVERSE
    </div>
  </footer>
}
