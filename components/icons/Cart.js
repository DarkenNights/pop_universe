export default function Cart({ className = '', width, height }) {
    return (
        <svg className={className} width={width} height={height} viewBox="0 0 48.69 48.69">
            <polygon className="icon-fill" points="9.86 10.23 45.59 10.23 38.89 28.62 13.8 28.62 9.86 10.23" />
            <path
                className="icon-stroke"
                d="M2,4.06H5.42L10.5,27a7.27,7.27,0,0,0-4.14,6.74A6.92,6.92,0,0,0,12.91,41h26.9a2,2,0,0,0,0-4.06H12.91a3,3,0,0,1-2.5-3.19,3,3,0,0,1,2.5-3.19h26.9a2,2,0,0,0,1.91-1.34l6.85-19a2,2,0,0,0-1.22-2.59,2,2,0,0,0-.69-.12H10.32L9,1.59A2,2,0,0,0,7.05,0H2A2,2,0,0,0,2,4.06ZM43.78,11.5l-5.39,15H14.54l-3.32-15Z"
            />
            <path className="icon-stroke" d="M14.55,44.63a2,2,0,1,0,0,4.06h2.57a2,2,0,1,0,0-4.06Z" />
            <path className="icon-stroke" d="M33.26,44.63a2,2,0,0,0,0,4.06h2.58a2,2,0,0,0,0-4.06Z" />
        </svg>
    )
}
