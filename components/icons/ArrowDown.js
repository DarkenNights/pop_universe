export default function ArrowDown({ className, width, height }) {
    return (
        <svg className={className} width={width} height={height} viewBox="0 0 451.85 257.57">
            <path
                className="icon-stroke"
                d="M225.92,257.57a31.57,31.57,0,0,1-22.37-9.27L9.27,54A31.64,31.64,0,0,1,54,9.27l171.9,171.91L397.83,9.27A31.64,31.64,0,0,1,442.57,54L248.29,248.31A31.56,31.56,0,0,1,225.92,257.57Z"
            />
        </svg>
    )
}
