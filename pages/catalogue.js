import Layout from '../components/Layout'
import styles from '../styles/pages/Catalogue.module.scss'
import Product from '../components/Product'
import Filter from '../components/icons/Filter'
import { useState } from 'react'
import CatalogFilters from '../components/sidebars/catalog/CatalogFilters'
import Pagination from '../components/Pagination'
import AssociatedSearch from '../components/AssociatedSearch'
import useTranslation from 'next-translate/useTranslation'

export default function Catalogue() {
    const { t, lang } = useTranslation('common')
    const [isFilterSidebarActive, setIsFilterSidebarActive] = useState(false)

    const toggleClass = () => {
        setIsFilterSidebarActive(!isFilterSidebarActive)
    }

    return (
        <Layout>
            <section className={styles['section-title']}>
                <div>Pop Culture Science-fiction (133)</div>
                <div className={styles['filters-button']} onClick={toggleClass}>
                    <Filter width={35} height={25} />
                </div>
            </section>
            <div className={styles['sidebar-container'] + ' ' + (isFilterSidebarActive ? styles.active : '')}>
                <CatalogFilters isFilterSidebarActive={isFilterSidebarActive} setIsFilterSidebarActive={setIsFilterSidebarActive} toggleClass={toggleClass} />
            </div>
            <section className={styles['section-catalog']}>
                <div className={styles.filters}>
                    <CatalogFilters />
                </div>
                <div className={styles.products}>
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                </div>
            </section>
            <section className={styles['section-pagination']}>
                <div className={styles.right}>
                    <hr />
                    <div className={styles['pagination-container']}>
                        <Pagination />
                    </div>
                </div>
            </section>
            <section className={styles['section-search']}>
                <div className={styles.right}>
                    <h3>{t('associatedSearches')}</h3>
                    <div className={styles['associated-searches-container']}>
                        <AssociatedSearch text={'Star Wars'} />
                        <AssociatedSearch text={'Star Wars'} />
                        <AssociatedSearch text={'Star Wars'} />
                    </div>
                </div>
            </section>
        </Layout>
    )
}
