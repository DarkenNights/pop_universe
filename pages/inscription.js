import Layout from '../components/Layout'
import { useState } from 'react'
import AuthService from '../services/authService'
import { useRouter } from 'next/router'
import { signIn } from 'next-auth/client'
import styles from '../styles/pages/Inscription.module.scss'
import FormAuth from '../components/forms/FormAuth'
import Checkbox from '../components/forms/Checkbox'
import Input from '../components/forms/Input'
import Link from 'next/link'

export default function Inscription() {
    const router = useRouter()
    const [fields, setFields] = useState({
        name: '',
        email: '',
        password: '',
    })
    const [error, setError] = useState(null)

    const handleChange = (event) => {
        setFields({
            ...fields,
            [event.target.name]: event.target.value,
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        try {
            //Création de l'utilisateur - Enpoint : /api/auth/register
            let response = await AuthService.register(name, email, password)
            //Si on a bien créé l'utilisateur
            if (response.status === 201) {
                //Connexion de l'utilisateur
                response = await signIn('credentials', {
                    redirect: false,
                    email: fields.name,
                    password: fields.password,
                })
                //Redirection vers la page des produits
                if (response.status === 200) {
                    await router.push('/')
                }
            }
        } catch (error) {
            setError(error.response.data)
        }
    }

    return (
        <Layout>
            <section className={styles['section-inscription']}>
                <div className={styles['text-container']}>
                    <h1>Créez un compte</h1>
                    <p>Collectionnez les meilleurs produits issus de la Pop Culture sur Pop Universe</p>
                </div>

                <FormAuth buttonText="Inscription" submitFunction={handleSubmit}>
                    {error && <div className="login-form-error">{error}</div>}
                    <Input type="text" name="name" placeholder="Name" onChange={handleChange} />
                    <Input type="text" name="email" placeholder="Email" onChange={handleChange} />
                    <Input type="password" name="password" placeholder="Password" onChange={handleChange} />
                    <div className={styles['register-cgu']}>
                        <Checkbox text="J'accepte les conditions générales d'utilisation" />
                    </div>
                </FormAuth>
                <div className={styles['register-account-container']}>
                    <div className={styles['register-account']}>
                        <div>Vous avez déjà un compte ?</div>
                        <div>
                            <Link href="/connexion">
                                <a>Connectez-vous !</a>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    )
}
