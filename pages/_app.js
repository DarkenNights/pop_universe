import '../styles/globals.scss'
import { Provider } from 'next-auth/client'
import Router from 'next/router'
import NProgress from 'nprogress'
import '../styles/vendors/nprogress.scss'
import '../styles/vendors/rec.scss'
import '../styles/vendors/slick.scss'
import '../styles/vendors/rc-slider.scss'

NProgress.configure({
    minimum: 0.3,
    easing: 'ease',
    speed: 800,
    showSpinner: false,
})

Router.events.on('routeChangeStart', () => NProgress.start())
Router.events.on('routeChangeComplete', () => NProgress.done())
Router.events.on('routeChangeError', () => NProgress.done())

export default function App({ Component, pageProps }) {
    return (
        <Provider session={pageProps.session}>
            <Component {...pageProps} />
        </Provider>
    )
}
