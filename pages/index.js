import Layout from '../components/Layout'
import Image from 'next/image'
import styles from '../styles/pages/Index.module.scss'
import Product from '../components/Product'
import Carousel from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import CarouselNextArrow from '../components/buttons/CarouselNextArrow'
import CarouselPrevArrow from '../components/buttons/CarouselPrevArrow'

export default function Index() {
    const settings = {
        arrows: false,
        dots: true,
        infinite: true,
        speed: 500,
        autoplay: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        nextArrow: <CarouselNextArrow />,
        prevArrow: <CarouselPrevArrow />,
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                },
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                },
            },
            {
                breakpoint: 374,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                },
            },
        ],
    }

    return (
        <Layout>
            <section>
                <div className={styles.slider}>
                    <Image src="/images/pages/index/slider_1.png" layout="fill" alt="Slider 1" objectFit="cover" />
                </div>
            </section>
            <section>
                <div className={styles['popculture-container']}>
                    <div className={styles['popculture']}>
                        <div className={styles['popculture-title']}>
                            Pop Culture <span>Japonaise</span>
                        </div>
                        <Image src={'/images/pop_culture/japan/japan_thumbnail.svg'} layout="responsive" width={408} height={197} alt="Pop culture japonaise" />
                        <div className={styles['popculture-text']}>Rajouter un super jolie texte pour décrire au maximum la pop culture dans les moindres détails</div>
                    </div>
                    <div className={styles['popculture']}>
                        <div className={styles['popculture-title']}>
                            Pop Culture <span>Comics</span>
                        </div>
                        <Image src={'/images/pop_culture/comics/comics_thumbnail.svg'} layout="responsive" width={408} height={197} alt="Pop culture comics" />
                        <div className={styles['popculture-text']}>Rajouter un super jolie texte pour décrire au maximum la pop culture dans les moindres détails</div>
                    </div>
                    <div className={styles['popculture']}>
                        <div className={styles['popculture-title']}>
                            Pop Culture <span>Fantasy</span>
                        </div>
                        <Image src={'/images/pop_culture/fantasy/fantasy_thumbnail.svg'} layout="responsive" width={408} height={197} alt="Pop culture fantasy" />
                        <div className={styles['popculture-text']}>Rajouter un super jolie texte pour décrire au maximum la pop culture dans les moindres détails</div>
                    </div>
                    <div className={styles['popculture']}>
                        <div className={styles['popculture-title']}>
                            Pop Culture <span>Retro</span>
                        </div>
                        <Image src={'/images/pop_culture/retro/retro_thumbnail.svg'} layout="responsive" width={408} height={197} alt="Pop culture retro" />
                        <div className={styles['popculture-text']}>Rajouter un super jolie texte pour décrire au maximum la pop culture dans les moindres détails</div>
                    </div>
                    <div className={styles['popculture']}>
                        <div className={styles['popculture-title']}>
                            Pop Culture <span>Gaming</span>
                        </div>
                        <Image src={'/images/pop_culture/gaming/gaming_thumbnail.svg'} layout="responsive" width={408} height={197} alt="Pop culture gaming" />
                        <div className={styles['popculture-text']}>Rajouter un super jolie texte pour décrire au maximum la pop culture dans les moindres détails</div>
                    </div>
                    <div className={styles['popculture']}>
                        <div className={styles['popculture-title']}>
                            Pop Culture <span>Science-Fiction</span>
                        </div>
                        <Image src={'/images/pop_culture/sci-fi/sci-fi_thumbnail.svg'} layout="responsive" width={408} height={197} alt="Pop culture science-fiction" />
                        <div className={styles['popculture-text']}>Rajouter un super jolie texte pour décrire au maximum la pop culture dans les moindres détails</div>
                    </div>
                </div>
            </section>
            <section className={styles['section-reinsurance']}>
                <div className={styles['reinsurance-container']}>
                    <h2>
                        Achetez, vendez et échangez tous les produits issus <br />
                        de la Pop Culture, en toute sécurité
                    </h2>
                    <div className={styles.reinsurances}>
                        <div className={styles.reinsurance}>
                            <div className={styles['reinsurance-icon']}>
                                <Image src={'/images/icons/shipping.svg'} width={25} height={25} alt="Icone de livraison" />
                            </div>
                            <div className={styles['reinsurance-title']}>Livraison Simple Et Rapide</div>
                            <div className={styles['reinsurance-text']}>Rajouter du texte concernant la livraison</div>
                        </div>
                        <div className={styles.reinsurance}>
                            <div className={styles['reinsurance-icon']}>
                                <Image src={'/images/icons/smiley.svg'} width={25} height={25} alt="Icone de livraison" />
                            </div>
                            <div className={styles['reinsurance-title']}>Nos clients sont satisfaits</div>
                            <div className={styles['reinsurance-text']}>Rajouter du texte concernant la satisfaction client</div>
                        </div>
                        <div className={styles.reinsurance}>
                            <div className={styles['reinsurance-icon']}>
                                <Image src={'/images/icons/cost.svg'} width={25} height={25} alt="Icone de livraison" />
                            </div>
                            <div className={styles['reinsurance-title']}>Nos collections sont certifiés</div>
                            <div className={styles['reinsurance-text']}>Rajouter du texte concernant la certification</div>
                        </div>
                        <div className={styles.reinsurance}>
                            <div className={styles['reinsurance-icon']}>
                                <Image src={'/images/icons/certify.svg'} width={25} height={25} alt="Icone de livraison" />
                            </div>
                            <div className={styles['reinsurance-title']}>Une qualité garantie</div>
                            <div className={styles['reinsurance-text']}>Rajouter du texte concernant la qualité</div>
                        </div>
                    </div>
                </div>
            </section>
            <section className={styles['section-products']}>
                <Carousel {...settings}>
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                    <Product />
                </Carousel>
            </section>
        </Layout>
    )
}
