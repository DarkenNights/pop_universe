import { getCsrfToken } from 'next-auth/client'
import Layout from '../components/Layout'
import styles from '../styles/pages/Connexion.module.scss'
import FormAuth from '../components/forms/FormAuth'
import Input from '../components/forms/Input'
import Link from 'next/link'
import Checkbox from '../components/forms/Checkbox'

export default function Connexion({ csrfToken }) {
    return (
        <Layout>
            <section className={styles['section-connection']}>
                <div className={styles['text-container']}>
                    <h1>Se connecter</h1>
                    <p>Collectionnez les meilleurs produits issus de la Pop Culture sur Pop Universe</p>
                </div>
                <FormAuth buttonText="Se connecter" action="/api/auth/callback/credentials">
                    <input name="csrfToken" type="hidden" defaultValue={csrfToken} />
                    <Input type="text" placeholder="Email" name="email" />
                    <Input type="password" placeholder="Password" name="password" />
                    <div className={styles['signin-remindme']}>
                        <Checkbox text="se souvenir de moi" />
                        <Link href="/mot-de-passe-oublier">
                            <a>Vous avez oublié votre mot de passe ?</a>
                        </Link>
                    </div>
                </FormAuth>
                <div className={styles['signin-noaccount-container']}>
                    <div className={styles['signin-noaccount']}>
                        <div>Vous n&apos;avez pas de compte ?</div>
                        <div>
                            <Link href="/inscription">
                                <a>Créez un compte</a>
                            </Link>
                        </div>
                    </div>
                </div>
            </section>
        </Layout>
    )
}

export async function getServerSideProps(context) {
    return {
        props: {
            csrfToken: await getCsrfToken(context),
        },
    }
}
