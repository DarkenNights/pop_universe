import sdk from 'aws-sdk'

sdk.config.update({
  accessKeyId: 'AKIATCTW4ER4W37S3RAV',
  //AKIATCTW4ER4W37S3RAV
  //TEST:AKIATCTW4ER46C2F7LLT
  secretAccessKey: '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',
  //yYSKXSRn99RHN6mgpTypdyCPcXk9CY2bwQPX8FDP
  //TEST: 53s6aKbRDhMRCwB08g82SIQeKRF7YcVVfPc3a30V
})

class AWS {
  S3_BUCKET = 'pop-collection'
  REGION = 'eu-west-3'

  myBucket = new sdk.S3({
    params: { Bucket: this.S3_BUCKET },
    region: this.REGION,
  })

  async uploadFile(file, folder) {
    const params = {
      ACL: 'public-read',
      Body: file,
      Bucket: this.S3_BUCKET + '/' + folder,
      Key: file.name,
    }

    const objectUrl =
      'https://' +
      this.S3_BUCKET +
      '.s3.' +
      this.REGION +
      '.amazonaws.com/' +
      folder +
      '/' +
      file.name

    // await this.myBucket.putObject(params).send((err, response) => {
    //   if (err) console.log(err)
    // })
    await this.myBucket.putObject(params, function (err, data) {
      if (err) console.log(err, err.stack)
      else console.log(data)
    })

    return objectUrl
  }

  async removeFile(fileName, folder) {
    const params = {
      Bucket: this.S3_BUCKET + '/' + folder,
      Key: fileName,
    }

    this.myBucket.deleteObject(params).send((err) => {
      if (err) console.log(err)
    })
  }
}

export default new AWS()
